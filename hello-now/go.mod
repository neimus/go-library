module gitlab.com/neimus/go-library/hello-now

go 1.12

require (
	github.com/beevik/ntp v0.2.0
	github.com/mailru/easyjson v0.0.0-20190626092158-b2ccc519800e
	github.com/stretchr/testify v1.4.0 // indirect
	golang.org/x/net v0.0.0-20190909003024-a7b16738d86b // indirect
)
