//Package service timeNow
package service

//go:generate easyjson $GOFILE

import (
	"time"

	"github.com/beevik/ntp"
)

// Default NTP server if not set
const defaultNtpServer string = "0.beevik-ntp.pool.ntp.org"

// TimeNow struct
//easyjson:json
type TimeNow struct {
	NtpServer string `json:"ntp_server"`
}

// GetNow returns exact time from NTP servers
func (t *TimeNow) GetNow() (time.Time, error) {

	if t.NtpServer == "" {
		t.NtpServer = defaultNtpServer
	}
	return ntp.Time(t.NtpServer)
}
