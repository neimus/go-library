// Package main
package main

import (
	"encoding/json"
	"fmt"
	"log"
	"os"
	"path/filepath"

	"gitlab.com/neimus/go-library/hello-now/pkg/service"
)

// Default config file
const configName string = "config.json"

func main() {
	timeNow := service.TimeNow{}
	loadConfiguration(&timeNow)

	now, err := timeNow.GetNow()
	if err != nil {
		log.Fatal(err)
	}

	fmt.Println(now.String())
}

//Load configuration (example: ntp server)
func loadConfiguration(timeNow *service.TimeNow) {
	file, err := os.Open(getConfigPath())
	if err != nil {
		logWarning("error loading configuration", err)
	} else {
		decoder := json.NewDecoder(file)

		if err := decoder.Decode(&timeNow); err != nil {
			logWarning("can not decode configuration", err)
		}
	}

	err = file.Close()
	if err != nil {
		log.Fatal(err)
	}
}

// return configuration path
func getConfigPath() string {
	dir, err := filepath.Abs(filepath.Dir(os.Args[0]))
	if err != nil {
		log.Fatal(err)
	}
	return dir + string(os.PathSeparator) + configName
}

// warning logging in stderr
func logWarning(msg string, err error) {
	_, _ = fmt.Fprintln(os.Stderr, msg, " {context: ", err, "}")
}
