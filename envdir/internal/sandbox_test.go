package sandbox_test

import (
	"errors"
	"log"
	"os"
	"strings"
	"testing"

	"github.com/stretchr/testify/assert"
	sandbox "gitlab.com/neimus/go-library/envdir/internal"
)

var validPathEnv = "./../.testdata"
var validEnv = map[string]string{
	"HOME_EMPTY":   "",
	"SANITIZE":     "TRUE",
	"TEST_VALUE":   "MacOs",
	"VALUE_SUBDIR": "recursive",
	"REPLACE_ME":   "subdir value",
}

type testOutput struct {
	buf []string
}

func (o *testOutput) Write(p []byte) (n int, err error) {
	o.buf = append(o.buf, string(p))
	return len(p), nil
}

func (o *testOutput) GetOutput() []string {
	return o.buf
}

func (o *testOutput) ClearOutput() {
	o.buf = []string{}
}

func TestReadDir(t *testing.T) {
	testCases := []struct {
		name          string
		pathEnv       string
		expectedValue map[string]string
		isErr         bool
	}{
		{
			name:          "positive_case",
			pathEnv:       validPathEnv,
			expectedValue: validEnv,
			isErr:         false,
		},
		{
			name:          "not_valid_path",
			pathEnv:       "path",
			expectedValue: make(map[string]string),
			isErr:         true,
		},
	}

	for _, testCase := range testCases {
		t.Run(testCase.name, func(t *testing.T) {
			envs, err := sandbox.ReadDir(testCase.pathEnv)
			if testCase.isErr {
				assert.Error(t, err)
			} else {
				assert.NoError(t, err)
			}

			assert.Equal(t, testCase.expectedValue, envs)
		})
	}
}

func TestPrepareEnvs(t *testing.T) {
	clearEnv()
	if err := os.Setenv("HOME_EMPTY", "remove"); err != nil {
		log.Fatal("failed to set HOME to environment variables")
	}

	envs, err := sandbox.PrepareEnvs(validEnv)
	assert.NoError(t, err)

	equalEnvs(t, getExpectedEnv(), envs)
}

func TestPrepareCommand(t *testing.T) {
	clearEnv()
	envs, err := sandbox.ReadDir(validPathEnv)
	assert.NoError(t, err)

	prepareEnvs, err := sandbox.PrepareEnvs(envs)
	assert.NoError(t, err)

	cmd := sandbox.PrepareCommand("buf", []string{"test"}, prepareEnvs)
	assert.Equal(t, "buf", cmd.Args[0])
	assert.Equal(t, "test", cmd.Args[1])

	equalEnvs(t, getExpectedEnv(), cmd.Env)
	assert.Equal(t, os.Stdout, cmd.Stdout)
	assert.Equal(t, os.Stdin, cmd.Stdin)
	assert.Equal(t, os.Stderr, cmd.Stderr)
}

func TestRunCmd(t *testing.T) {
	clearEnv()

	expectedEnv := getExpectedEnv()
	testCases := []struct {
		name          string
		command       string
		args          []string
		expectedValue map[string]string
		expectedErr   error
		expectedCode  int
		isErr         bool
	}{
		{
			name:          "positive_case",
			command:       "env",
			args:          []string{},
			expectedValue: expectedEnv,
			expectedCode:  0,
			isErr:         false,
		},
		{
			name:    "args",
			command: "env",
			args:    []string{"-i", "PATH=" + os.Getenv("PATH"), "env"},
			expectedValue: map[string]string{
				"PATH": os.Getenv("PATH"),
			},
			expectedCode: 0,
			isErr:        false,
		},
		{
			name:         "failed start command",
			command:      "bad_commands",
			expectedCode: 1,
			expectedErr:  sandbox.ErrStartCommand,
			isErr:        true,
		},
		{
			name:         "failed end command",
			command:      "env",
			args:         []string{"-badargs"},
			expectedCode: 1,
			expectedErr:  sandbox.ErrEndCommand,
			isErr:        true,
		},
	}

	envs, err := sandbox.ReadDir(validPathEnv)
	assert.NoError(t, err)

	prepareEnvs, err := sandbox.PrepareEnvs(envs)
	assert.NoError(t, err)

	stderr := new(testOutput)
	stdout := new(testOutput)

	for _, testCase := range testCases {
		t.Run(testCase.name, func(t *testing.T) {
			stderr.ClearOutput()
			stdout.ClearOutput()

			cmd := sandbox.PrepareCommand(testCase.command, testCase.args, prepareEnvs)
			cmd.Stdout = stdout
			cmd.Stderr = stderr

			code, err := sandbox.RunCmd(cmd)

			if testCase.isErr {
				if err != nil && testCase.expectedErr != nil && !errors.Is(err, testCase.expectedErr) {
					t.Errorf("\nExpected error: %s \nActual error: %s", testCase.expectedErr.Error(), err.Error())
				} else {
					assert.Error(t, err)
				}
			} else {
				assert.NoError(t, err)
				if testCase.expectedValue != nil {
					if len(stdout.GetOutput()) > 0 {
						equalEnvs(t, testCase.expectedValue, strings.Split(stdout.GetOutput()[0], "\n"))
					} else {
						t.Error("The command contains no output data.")
					}
				}
				if len(stderr.GetOutput()) > 0 {
					t.Errorf("Command output contains errors: %s", stderr.GetOutput()[0])
				}
			}

			if testCase.expectedCode > 0 && code == 0 {
				t.Errorf("\nExpected code > %d \nActual error: %d", testCase.expectedCode, code)
			} else if testCase.expectedCode == 0 && code > 0 {
				t.Errorf("\nExpected code == %d \nActual error: %d", testCase.expectedCode, code)
			}
		})
	}
}

func getExpectedEnv() map[string]string {
	expectedValue := make(map[string]string)
	expectedValue["PATH"] = os.Getenv("PATH")
	for k, v := range validEnv {
		if k == "HOME_EMPTY" {
			continue
		}
		expectedValue[k] = v
	}

	return expectedValue
}

func equalEnvs(t *testing.T, expectedEnv map[string]string, actualEnvs []string) {
	actualValue := make(map[string]string)
	for _, env := range actualEnvs {
		if env == "" {
			continue
		}
		d := strings.Split(env, "=")
		actualValue[d[0]] = d[1]
	}

	assert.Equal(t, expectedEnv, actualValue)
}

func clearEnv() {
	path := os.Getenv("PATH")
	os.Clearenv()
	err := os.Setenv("PATH", path)
	if err != nil {
		log.Fatal("failed to set PATH to environment variables")
	}
}
