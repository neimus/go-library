//Package sandbox - running commands in a sandbox with a variable environment set
package sandbox

import (
	"errors"
	"fmt"
	"io/ioutil"
	"log"
	"os"
	"os/exec"
	"path/filepath"
	"strings"
)

//ErrStartCommand - error occurring when running the command
var ErrStartCommand = errors.New("failed to start command")

//ErrEndCommand - error occurring during the execution of the command
var ErrEndCommand = errors.New("failed to end command")

//ReadDir - scans the specified directory and returns all environment variables defined in it
func ReadDir(dir string) (map[string]string, error) {
	var (
		envs = make(map[string]string)
		err  error
	)

	if dir, err = filepath.Abs(dir); err != nil {
		return envs, err
	}

	infos, err := ioutil.ReadDir(dir)
	if err != nil {
		return envs, err
	}

	for _, fileInfo := range infos {
		path := filepath.Join(dir, fileInfo.Name())

		//we are looking for all the files in the subdirectories
		if fileInfo.IsDir() {
			addedEnvs, err := ReadDir(path)
			if err != nil {
				return envs, nil
			}
			mergeEnvs(envs, addedEnvs)
			continue
		}

		//read the file and fill in envs
		data, err := ioutil.ReadFile(path)
		if err != nil {
			return envs, err
		}
		if validateKey(fileInfo.Name()) {
			envs[fileInfo.Name()] = sanitizeValue(string(data))
		} else {
			log.Printf("invalid key %s detected, value will not be applied", fileInfo.Name())
		}
	}

	return envs, nil
}

//PrepareEnvs - prepares environment variables (sets or unset)
func PrepareEnvs(inputEnvs map[string]string) ([]string, error) {
	for key, value := range inputEnvs {
		if value == "" {
			if err := os.Unsetenv(key); err != nil {
				return nil, fmt.Errorf("error unset buf %w", err)
			}
		} else {
			if err := os.Setenv(key, value); err != nil {
				return nil, fmt.Errorf("error set buf %w", err)
			}
		}
	}

	return os.Environ(), nil
}

// PrepareCommand - prepares the program for launch (redirects input and output, set environments)
func PrepareCommand(command string, args []string, env []string) *exec.Cmd {
	cmd := exec.Command(command, args...)
	cmd.Env = env
	cmd.Stdout = os.Stdout
	cmd.Stderr = os.Stderr
	cmd.Stdin = os.Stdin

	return cmd
}

//RunCmd - starts a program with arguments with an overridden environment
func RunCmd(command *exec.Cmd) (int, error) {
	if err := command.Start(); err != nil {
		return 1, fmt.Errorf("%w: %s", ErrStartCommand, err.Error())
	}

	if err := command.Wait(); err != nil {
		execErr, ok := err.(*exec.ExitError)
		if ok {
			return execErr.ExitCode(), fmt.Errorf("%w: %s", ErrEndCommand, err.Error())
		}
		return 1, fmt.Errorf("%w: %s", ErrEndCommand, err.Error())
	}

	return 0, nil
}

func mergeEnvs(originalEnvs, addedEnvs map[string]string) map[string]string {
	for key, value := range addedEnvs {
		originalEnvs[key] = value
	}

	return originalEnvs
}

func validateKey(key string) bool {
	// todo implement validator
	return true
}

func sanitizeValue(value string) string {
	return strings.TrimSpace(value)
}
