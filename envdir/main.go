// Package main
package main

import (
	"fmt"
	"os"

	"gitlab.com/neimus/go-library/envdir/internal"

	"github.com/logrusorgru/aurora"
)

const maxCountCmdArgs = 50

type inputArgs struct {
	EnvDir  string
	Cmd     string
	CmdArgs []string
}

func main() {
	args := parseArgs()
	envs, err := sandbox.ReadDir(args.EnvDir)
	if err != nil {
		exitWithError(fmt.Errorf("error read dir %w", err), 1)
	}

	prepareEnvs, err := sandbox.PrepareEnvs(envs)
	if err != nil {
		exitWithError(err, 1)
	}

	code, err := sandbox.RunCmd(sandbox.PrepareCommand(args.Cmd, args.CmdArgs, prepareEnvs))
	if err != nil {
		exitWithError(err, code)
	}
}

func parseArgs() inputArgs {
	nArgs := len(os.Args)
	if nArgs < 3 {
		exitWithError(fmt.Errorf("no required arguments specified"), 1)
	}

	countCmdArgs := nArgs - 3
	if countCmdArgs > maxCountCmdArgs {
		exitWithError(fmt.Errorf("the number of transmitted arguments for the team cannot exceed %d", maxCountCmdArgs), 1)
	}

	args := inputArgs{}
	if countCmdArgs > 0 {
		args.CmdArgs = make([]string, 0, countCmdArgs)
	}

	for index, arg := range os.Args[1:] {
		switch index {
		case 0:
			args.EnvDir = arg
		case 1:
			args.Cmd = arg
		default:
			args.CmdArgs = append(args.CmdArgs, arg)
		}
	}

	return args
}

func exitWithError(err error, code int) {
	if code == 1 {
		printHelp()
	}

	fmt.Println(aurora.Bold(aurora.Red(err)))
	os.Exit(code)
}

func printHelp() {
	cmd := "envdir"
	description := "runs another program with environment modified according to files in a specified directory"
	args := map[string]string{
		"dir":  "directory where env files are located. The name is the key, and the content is the value",
		"cmd":  "command to run with redefined environment variables",
		"args": "(optional) arguments for the command to run",
	}

	fmt.Println("NAME:")
	fmt.Printf("\t%s - %s\n", aurora.Blue(cmd), aurora.Gray(20, description))

	fmt.Println("SYNOPSIS:")
	fmt.Printf("\t%s %s\n", aurora.Blue(cmd), aurora.Green("dir cmd args"))

	fmt.Println("ARGUMENTS:")
	for arg, description := range args {
		fmt.Printf("\t%s\t-%s\n", aurora.Green(arg), aurora.Gray(20, description))
	}
}
