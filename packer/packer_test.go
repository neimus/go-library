package packer

import "testing"

func TestUnPack(t *testing.T) {
	data := map[string]string{
		`qwe\45\3`: `qwe444443`,
		`a4bc2d5e`: `aaaabccddddde`,
		`abcd`:     `abcd`,
		`\45`:      `44444`,
		`\40`:      `40`,
		`0a10`:     `0a0`,
		`a12b`:     `aaaaaaaaaaaab`,
		`qwe\\5`:   `qwe\\\\\`,
		`\\`:       `\`,
		``:         ``,
	}

	for checked, expected := range data {
		result, err := UnPack(checked)
		if result != expected || err != nil {
			t.Errorf("chError UnPack: %s | error: %v", checked, err)
		}
	}
}

func TestErrorUnPack(t *testing.T) {
	data := []string{
		"45",
		`\`,
		"a99999b",
	}

	for _, checked := range data {
		result, err := UnPack(checked)
		if result != "" || err == nil {
			t.Errorf("chError UnPack: %s | error: %v", checked, err)
		}
	}
}
