//Package packer for unpacking and packing strings
package packer

import (
	"gitlab.com/neimus/go-library/packer/pkg/service"
)

// UnPack unpacking strings
func UnPack(data string) (string, error) {
	decoder := service.Decoder{}

	for _, symbol := range data {
		if err := decoder.AddSymbol(symbol); err != nil {
			return "", err
		}
	}

	return decoder.GetText()
}
