//Package service for decoding a string
package service

import (
	"fmt"
	"strconv"
	"strings"
)

// Decoder struct
type Decoder struct {
	escape     bool
	lastSymbol rune
	count      int
	result     []string
	repeat     []string
}

// AddSymbol handle for each symbol
func (decoder *Decoder) AddSymbol(currentSymbol rune) error {
	if currentSymbol > 0 {
		decoder.count++
	}

	if err := decoder.handleLastSymbol(currentSymbol); err != nil {
		return err
	}

	if err := decoder.handleCurrentSymbol(currentSymbol); err != nil {
		return err
	}

	return nil
}

func (decoder *Decoder) handleLastSymbol(currentSymbol rune) error {
	if decoder.lastSymbol > 0 && !decoder.isRepeat(currentSymbol) {
		if decoder.repeat != nil {
			repeat, err := strconv.Atoi(strings.Join(decoder.repeat, ""))
			if err != nil {
				return fmt.Errorf("error converting string to number")
			}
			decoder.save(strings.Repeat(string(decoder.lastSymbol), repeat))
		} else {
			decoder.save(string(decoder.lastSymbol))
		}
	} else if decoder.escape && currentSymbol == 0 {
		return fmt.Errorf("unexpected end of line when escaping character on line %d", decoder.count)
	}

	return nil
}

func (decoder *Decoder) handleCurrentSymbol(currentSymbol rune) error {
	if decoder.escape {
		decoder.lastSymbol = currentSymbol
		decoder.escape = false
	} else if decoder.isRepeat(currentSymbol) {
		if decoder.lastSymbol == 0 {
			return fmt.Errorf(
				"in the decoded count there is an invalid character %s on line %d",
				string(currentSymbol), decoder.count)
		}
		if len(decoder.repeat) >= 4 {
			return fmt.Errorf("maximum possible factor is 9999")
		}
		decoder.repeat = append(decoder.repeat, string(currentSymbol))
	} else if decoder.isEscape(currentSymbol) {
		decoder.escape = true
	} else {
		decoder.lastSymbol = currentSymbol
	}

	return nil
}

// GetText returns a decoded string
func (decoder *Decoder) GetText() (string, error) {
	if err := decoder.handleLastSymbol(0); err != nil {
		return "", err
	}
	return strings.Join(decoder.result, ""), nil
}

func (decoder *Decoder) isRepeat(symbol rune) bool {
	// 48 (ноль) - считаем валидным символом
	return symbol >= 49 && symbol <= 57
}

func (decoder *Decoder) isEscape(symbol rune) bool {
	return symbol == 92
}

func (decoder *Decoder) save(text string) {
	decoder.result = append(decoder.result, text)
	decoder.repeat = nil
	decoder.lastSymbol = 0
}
