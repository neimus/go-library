all: help

## Current path
PATH_CURRENT = $(shell pwd)
.PHONY: project
project:
	@read -p "Enter project name : " PROJECT; \
	if [[ -z "$$PROJECT" ]] || [[ -d "$$PROJECT" ]]; then \
	 	printf "\033[31mProject already exists\033[0m \n"; \
	 	exit 1; \
	 else \
	 	mkdir $$PROJECT; \
	 	echo "include ../.make/Makefile.base" >> $$PROJECT/Makefile; \
		$(MAKE) -C $$PROJECT init; \
		printf "\033[36mProject created!\033[0m \n"; \
	 fi

.PHONY: install dependency tools tools-test
dependency:
	@ERR=0 && \
    for d in */; do \
    	printf "\033[36mProject $$d\033[0m %s\n" && \
    	make -C $${d} dependency; \
    	ERR=$$? && \
		if [ $$ERR != 0 ]; then \
			exit $$ERR; \
		fi \
    done

install:
	@ERR=0 && \
	for d in */; do \
    	printf "\033[36mProject $$d\033[0m %s\n" && \
    	make -C $${d} install; \
    	ERR=$$? && \
		if [ $$ERR != 0 ]; then \
			exit $$ERR; \
		fi \
    done

tools: ## Install all tools for project
	@export GO111MODULE=off && \
	export GOFLAGS="" && \
	echo "Install easyjson"; \
	go get -u github.com/mailru/easyjson/...; \
	echo "Install golint"; \
    go get -u -t golang.org/x/lint/golint; \
    echo "Install goimports"; \
    go get -u -t golang.org/x/tools/cmd/goimports; \
    echo "Install golangci-lint"; \
    go get -u github.com/golangci/golangci-lint/cmd/golangci-lint; \
    echo "Install gosec"; \
    go get -u github.com/securego/gosec/cmd/gosec

tools-test:
	@export GO111MODULE=off && \
	export GOFLAGS="" && \
	echo "Install go-junit-report"; \
	go get -u github.com/jstemmer/go-junit-report

.PHONY: test test-gitlab-ci test-coverage-gitlab-ci
test:
	@ERR=0 && \
	for d in */; do \
    	printf "\033[36mProject $$d\033[0m %s\n" && \
    	make -C $${d} test; \
    	ERR=$$? && \
		if [ $$ERR != 0 ]; then \
			exit $$ERR; \
		fi \
    done

test-gitlab-ci:
	@ERR=0 && \
	rm -f $(PATH_CURRENT)/tmp.out && \
    for d in */; do \
    	cd $(PATH_CURRENT)/$${d}/ && go test -v ./... 1>>./../tmp.out 2>>./../tmp.out; \
    	ERR=$$? && \
		if [ $$ERR != 0 ]; then \
			cat $(PATH_CURRENT)/tmp.out | go-junit-report; \
			exit $$ERR; \
		fi \
    done; \
    cat $(PATH_CURRENT)/tmp.out | go-junit-report; \
    rm -f $(PATH_CURRENT)/tmp.out

test-coverage-gitlab-ci:
	@echo "Run unit tests with coverage profile"; \
	for d in */; do \
		make -C $${d} test-coverage-gitlab-ci; \
	done

.PHONY: lint lint-format lint-style lint-ci
lint:
	@ERR=0 && \
	for d in */; do \
    	printf "\033[36mProject $$d\033[0m %s\n" && \
    	make -C $${d} lint; \
    	ERR=$$? && \
		if [ $$ERR != 0 ]; then \
			exit $$ERR; \
		fi \
    done

lint-format:
	@ERR=0 && \
	for d in */; do \
    	printf "\033[36mProject $$d\033[0m %s\n" && \
    	make -C $${d} lint-format; \
    	ERR=$$? && \
		if [ $$ERR != 0 ]; then \
			exit $$ERR; \
		fi \
    done

lint-style:
	@ERR=0 && \
	for d in */; do \
     	printf "\033[36mProject $$d\033[0m %s\n" && \
    	make -C $${d} lint-style; \
    	ERR=$$? && \
		if [ $$ERR != 0 ]; then \
			exit $$ERR; \
		fi \
    done

lint-ci:
	@ERR=0 && \
	for d in */; do \
    	printf "\033[36mProject $$d\033[0m %s\n" && \
    	make -C $${d} lint-ci; \
    	ERR=$$? && \
		if [ $$ERR != 0 ]; then \
			exit $$ERR; \
		fi \
    done

.PHONY: help
help:
	@printf "\nCommands:\n"; \
	printf "\033[36m make project\033[0m  create a project\n"; \
	for i in $$(ls -d */); do printf "\033[36m make -C %s\033[0m  run project commands '%s'\n" $${i%%/} $${i%%/}; done
	@printf "\n"
