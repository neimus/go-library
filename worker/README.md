## Worker
Библиотека для параллельного выполнения N заданий (т.е. в N параллельных горутинах)

### Установка
```bash
export GO111MODULE=on && go get -u gitlab.com/neimus/go-library/worker
```

### Использование
* Инициализируйте worker указав кол-во запускаемых горутин и максимальное кол-во ошибок
> ВНИМАНИЕ! Количесто обработанных задач может быть больше, т.к. ошибка возвращается после обработки функции
```go
    worker := Worker{}
    worker.Init(3, 1)
``` 

* Создайте необходимое кол-во задач
```go
tasks := make([]func() error, 0, 3)
tasks = append(tasks, exampleTask)
tasks = append(tasks, exampleTask)
tasks = append(tasks, exampleTask)

func exampleTask() error {
	time.Sleep(10 * time.Microsecond)
	return nil
}
``` 

* Добавьте обработку сигнала ОС, если это необходимо
```go
worker.TerminateOnSysCall(syscall.SIGTERM)
``` 

* Запустите обработку задач
```go
countProcessed, errors := worker.RunTasks(tasks)
// где countProcessed - кол-во обработанных задач
// errors - слайс ошибок []error
```


### Основные make-команды:
 * `make tools` - установка инструментов для разработки и не только (linters, easyjson ...)
 * `make test` - запуск тестов и проверка покрытия кода тестами
 * `make lint` - запуск проверки code style и ошибок в коде (перед запуском необходимо установить все инструменты `make tools`)
 * `make clean-all` - удаляет файлы созданные easyjson, coverage и очищает все зависимости `$GOPATH/pkg/mod`
 * `make tidy` - алиас для `go mod tidy`
 * `make` или `make help` - вызов справки по командам
