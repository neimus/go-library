package worker

import (
	"fmt"
	"testing"
	"time"
)

func TestWorker_DefaultRunTasks(t *testing.T) {
	worker := Worker{}
	worker.Init(3, 1)

	tasks := make([]func() error, 0, 6)
	tasks = append(tasks, functionWithoutError)
	tasks = append(tasks, functionWithoutError)
	tasks = append(tasks, functionWithoutError)
	tasks = append(tasks, functionWithoutError)
	tasks = append(tasks, functionWithoutError)
	tasks = append(tasks, functionWithError)

	countProcessed, errors := worker.RunTasks(tasks)

	if countProcessed != 6 || len(errors) != 1 {
		printErrors(t, errors)
		t.Errorf("not all tasks were processed, {countProcessed: %d, countErrors: %d}",
			countProcessed, len(errors))
	}
}

func TestWorker_CheckMaxError(t *testing.T) {
	worker := Worker{}
	worker.Init(2, 3)

	tasks := make([]func() error, 0, 14)
	tasks = append(tasks, functionWithoutError)
	tasks = append(tasks, functionWithError)
	tasks = append(tasks, functionWithError)
	tasks = append(tasks, functionWithError)
	tasks = append(tasks, functionWithError)
	tasks = append(tasks, functionWithError)
	tasks = append(tasks, functionWithError)
	tasks = append(tasks, functionWithError)
	tasks = append(tasks, functionWithError)
	tasks = append(tasks, functionWithError)
	tasks = append(tasks, functionWithError)
	tasks = append(tasks, functionWithError)
	tasks = append(tasks, functionWithoutError)
	tasks = append(tasks, functionWithoutError)

	countProcessed, errors := worker.RunTasks(tasks)

	// Обработчик заданий должен вернуть не более 3 ошибок
	// Количесто обработанных задач может быть больше, так как ошибка возвращается после обработки функции
	if countProcessed == 14 || len(errors) != 3 {
		printErrors(t, errors)
		t.Errorf("more allowed tasks (%d) were launched with a maximum number of errors equal to %d",
			countProcessed, 3)
	}
}

func TestWorker_NotInit(t *testing.T) {
	worker := Worker{}

	tasks := make([]func() error, 0, 3)
	tasks = append(tasks, functionWithoutError)
	tasks = append(tasks, functionWithoutError)
	tasks = append(tasks, functionWithError)

	countProcessed, errors := worker.RunTasks(tasks)

	if countProcessed != 0 || len(errors) != 1 {
		printErrors(t, errors)
		t.Errorf("uninitialized worker was started")
	}
}

func functionWithError() error {
	time.Sleep(10 * time.Microsecond)
	return fmt.Errorf("opps -> function return error")
}

func functionWithoutError() error {
	time.Sleep(10 * time.Microsecond)
	return nil
}

func printErrors(t *testing.T, errors []error) {
	for _, err := range errors {
		t.Log(err)
	}
}
