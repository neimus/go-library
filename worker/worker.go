// Package worker - parallel execution of N tasks (in N parallel goroutines)
package worker

import (
	"fmt"
	"os"
	"os/signal"
	"sync"
)

// Worker parallel execution of N tasks
type Worker struct {
	maxErrors      int
	countErrors    int
	countHandlers  int
	countProcessed int
	errors         []error
	interrupt      chan os.Signal
	chTasks        chan func() error
	mutex          sync.RWMutex
	wg             sync.WaitGroup
}

// Init Worker initialization
// goroutines - number of parallel goroutines
// maxErrors - maximum number of errors after which processing will be stopped
// Attention! The number of processed tasks may be greater, since the error is returned after processing the function
func (w *Worker) Init(goroutines int, maxErrors int) {
	w.countHandlers = goroutines
	w.maxErrors = maxErrors
	w.mutex = sync.RWMutex{}
	w.errors = make([]error, 0, maxErrors)
	w.interrupt = make(chan os.Signal, 1)
}

// TerminateOnSysCall sets the OS signal to stop all tasks
func (w *Worker) TerminateOnSysCall(sig os.Signal) {
	signal.Notify(w.interrupt, os.Interrupt, sig)
}

// RunTasks performs parallel tasks
func (w *Worker) RunTasks(tasks []func() error) (int, []error) {
	if w.countHandlers <= 0 {
		return 0, []error{fmt.Errorf("worker was not initialized")}
	}
	size := 10
	if size > len(tasks) {
		size = len(tasks)
	}

	w.chTasks = make(chan func() error, size)

	for i := 0; i <= w.countHandlers; i++ {
		w.createHandle()
	}

	w.handleTasks(tasks)

	return w.countProcessed, w.errors
}

func (w *Worker) allowRun() bool {
	w.mutex.RLock()
	defer w.mutex.RUnlock()
	return w.countErrors < w.maxErrors
}

func (w *Worker) createHandle() {
	w.wg.Add(1)
	go func(w *Worker) {
		for task := range w.chTasks {
			select {
			case <-w.interrupt:
				w.addError(fmt.Errorf("program was interrupted by the user"))
				break
			default:
				if !w.allowRun() {
					break
				}
				if err := task(); err != nil {
					w.addError(err)
				}
				w.mutex.Lock()
				w.countProcessed++
				w.mutex.Unlock()
			}
		}
		w.wg.Done()
	}(w)
}

func (w *Worker) handleTasks(tasks []func() error) {
	for _, task := range tasks {
		w.chTasks <- task
	}
	close(w.chTasks)

	w.wg.Wait()
}

func (w *Worker) addError(err error) {
	w.mutex.Lock()
	defer w.mutex.Unlock()
	w.countErrors++

	if w.countErrors <= w.maxErrors {
		w.errors = append(w.errors, err)
	}
}
