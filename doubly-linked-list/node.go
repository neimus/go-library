// Package list - a related data structure that consists
// of a set of sequentially related records called nodes.
package list

// INode - интерфейс для узла списка
type INode interface {
	//PushFront - добавляет новое значение перед текущим узлом
	PushFront(interface{})
	//PushBack - добавляет новое значение после текущего узла
	PushBack(interface{})
	//Remove - удаляет текущий узел
	Remove()
	//Prev - возвращает предыдущий узел
	Prev() INode
	//Next - возвращает ссылку на следущий узел
	Next() INode
	//Value - Возвращает значение узла
	Value() interface{}
	//SetValue - Задает или изменяет значение в узле
	SetValue(interface{})
}

// Node - узел списка
type Node struct {
	list  *DLinkedList
	prev  *Node
	next  *Node
	value interface{}
}

//PushFront - добавляет новое значение перед текущим узлом
func (n *Node) PushFront(value interface{}) {
	n.list.addNodeAfter(value, n)
}

//PushBack - добавляет новое значение после текущего узла
func (n *Node) PushBack(value interface{}) {
	n.list.addNodeBefore(value, n)
}

//Remove - удаляет текущий узел
func (n *Node) Remove() {
	n.list.removeNode(n)
}

//Prev - возвращает предыдущий узел
func (n *Node) Prev() INode {
	return n.prev
}

//Next - возвращает ссылку на следущий узел
func (n *Node) Next() INode {
	return n.next
}

//Value - Возвращает значение узла
func (n *Node) Value() interface{} {
	return n.value
}

//SetValue - Задает или изменяет значение в узле
func (n *Node) SetValue(value interface{}) {
	n.value = value
}
