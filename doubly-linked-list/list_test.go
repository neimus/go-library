// Package list
package list

import "testing"

func TestCreateFirstElement(t *testing.T) {
	const value = "First"
	list := make([]IList, 6)

	list[0] = New()
	list[0].Last().SetValue(value)

	list[1] = New()
	list[1].Last().PushBack(value)

	list[2] = New()
	list[2].Last().PushFront(value)

	list[3] = New()
	list[3].First().SetValue(value)

	list[4] = New()
	list[4].First().PushBack(value)

	list[5] = New()
	list[5].First().PushFront(value)

	for i, l := range list {
		if l.First().Value() != value {
			t.Errorf("Failed to set the first item in %d case", i)
		}
	}
}

func TestOrderElement(t *testing.T) {
	list := New()
	list.First().SetValue("MiddleElement")
	list.Last().PushFront("LastElement")
	list.Last().Prev().PushBack("FirstElement")

	list.Last().PushFront("DeleteElement")
	list.Last().Remove()

	if list.First().Value() != "FirstElement" && list.First().Next() == nil {
		t.Error("Failed to set the first element")
	}
	if list.Last().Value() != "LastElement" && list.Last().Prev() == nil {
		t.Error("Failed to set the last element")
	}
	if list.First().Next().Value() != "MiddleElement" &&
		list.First().Next().Next() == list.Last() &&
		list.First().Next().Prev() == list.First() {
		t.Error("Failed to set the middle element")
	}

	if list.Len() != 3 {
		t.Error("Invalid long list items")
	}
}

func TestRemoveElement(t *testing.T) {
	list := New()
	for i := 0; i < 3; i++ {
		list.First().PushFront(struct{}{})
	}

	list.Last().Remove()

	if list.Len() != 2 {
		t.Error("Node was deleted")
	}
}
