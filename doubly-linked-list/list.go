// Package list - a related data structure that consists
// of a set of sequentially related records called nodes.
package list

import "sync"

// IList интерфейс для двухсвязного списка
type IList interface {
	//Len - длинна списка
	Len() uint64
	//First - возвращает первый узел списка
	First() INode
	//Last - возвращает последний узел списка
	Last() INode
}

// DLinkedList структура двухсвязанных данных
type DLinkedList struct {
	len       uint64
	firstNode *Node
	lastNode  *Node
	mutex     sync.Mutex
}

// New - Создание нового двухсвязного списка
func New() IList {
	list := DLinkedList{
		len:       0,
		firstNode: nil,
		lastNode:  nil,
	}

	return &list
}

//Len - длинна списка
func (l *DLinkedList) Len() uint64 {
	return l.len
}

//First - возвращает первый узел списка
func (l *DLinkedList) First() INode {
	if l.len == 0 {
		l.firstNode = l.createNode(nil, nil, nil)
	}
	return l.firstNode
}

//Last - возвращает последний узел списка
func (l *DLinkedList) Last() INode {
	if l.len == 0 {
		l.firstNode = l.createNode(nil, nil, nil)
	}

	if l.lastNode == nil {
		return l.firstNode
	}

	return l.lastNode
}

func (l *DLinkedList) addNodeAfter(value interface{}, currentNode *Node) {
	l.checkEmptyParent(currentNode.next, currentNode)

	newNode := l.createNode(value, currentNode, currentNode.next)

	l.mutex.Lock()
	defer l.mutex.Unlock()
	if currentNode.next != nil {
		currentNode.next.prev = newNode
	}
	currentNode.next = newNode

	l.refreshRoots(newNode)
	l.refreshRoots(currentNode)
}

func (l *DLinkedList) addNodeBefore(value interface{}, currentNode *Node) {
	l.checkEmptyParent(currentNode, currentNode.prev)

	newNode := l.createNode(value, currentNode.prev, currentNode)

	l.mutex.Lock()
	defer l.mutex.Unlock()
	if currentNode.prev != nil {
		currentNode.prev.next = newNode
	}
	currentNode.prev = newNode

	l.refreshRoots(newNode)
	l.refreshRoots(currentNode)
}

func (l *DLinkedList) checkEmptyParent(next *Node, prev *Node) {
	if (l.firstNode != nil || l.lastNode != nil) && prev == nil && next == nil {
		panic("Trying to add an item to without specifying parent references")
	}
}

func (l *DLinkedList) createNode(value interface{}, prev *Node, next *Node) *Node {
	// Если есть пустая нода - заполняем ее и возвращаем как новую
	if l.firstNode != nil && l.firstNode.prev == nil &&
		l.firstNode.next == nil && l.firstNode.value == nil {
		l.firstNode.prev = prev
		l.firstNode.next = next
		l.firstNode.value = value

		return l.firstNode
	}

	node := Node{
		list:  l,
		prev:  prev,
		next:  next,
		value: value,
	}
	l.len++

	return &node
}

func (l *DLinkedList) removeNode(node *Node) {
	l.mutex.Lock()
	defer l.mutex.Unlock()
	if node.prev != nil {
		node.prev.next = node.next
	}

	if node.next != nil {
		node.next.prev = node.prev
	}
	node.next = nil
	node.prev = nil
	node.list = nil
	l.len--
}

func (l *DLinkedList) refreshRoots(node *Node) {
	if node.prev == nil {
		l.firstNode = node
	}
	if node.next == nil {
		l.lastNode = node
	}
}
