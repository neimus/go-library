package textanalysis

import (
	"testing"
)

func TestFrequencyAnalysis_Unicode(t *testing.T) {
	runTestFrequencyAnalysis(t, map[string][]map[string]int{
		"Тест юникода. Тест должен показать 3 слова тест. И 2 слова": {
			{"тест": 3},
			{"слова": 2},
			{"должен": 1},
			{"и": 1},
			{"показать": 1},
			{"юникода": 1},
		},
	}, 10)
}

func TestFrequencyAnalysis_EmptyText(t *testing.T) {
	runTestFrequencyAnalysis(t, map[string][]map[string]int{
		"": {},
	}, 10)
	runTestFrequencyAnalysis(t, map[string][]map[string]int{
		"___ - ___ _": {},
	}, 10)
}

func TestFrequencyAnalysis_TextWithPunctuation(t *testing.T) {
	runTestFrequencyAnalysis(t, map[string][]map[string]int{
		"+_+ Adort, blick~^^^ @crowle$$$ \tadort!!!   \n": {
			{`adort`: 2},
			{`blick`: 1},
			{`crowle`: 1},
		},
	}, 10)
}

func TestFrequencyAnalysis_SmallText(t *testing.T) {
	runTestFrequencyAnalysis(t, map[string][]map[string]int{
		`Adort blick crowle adort`: {
			{`adort`: 2},
			{`blick`: 1},
			{`crowle`: 1},
		},
	}, 10)
}

func TestFrequencyAnalysis_Order(t *testing.T) {
	runTestFrequencyAnalysis(t, map[string][]map[string]int{
		`Ado_rt blick crowle adort brown crowle blick root brown`: {
			{`adort`: 2},
			{`blick`: 2},
			{`brown`: 2},
			{`crowle`: 2},
			{`root`: 1},
		},
	}, 5)
}

func TestFrequencyAnalysis_BigText(t *testing.T) {
	runTestFrequencyAnalysis(t, map[string][]map[string]int{
		`Phasellus euismod leo, Phasellus quis auctor, (@auCtor)!! 
		 Phasellus metus cursusleo Morbi blandit ac purus, purus. 
		 Integer blandit purus facilisis leo miphasellus, finibus 
		 tempus leo neque molestie. iaculis velit nulla vitae nulla.
		 *%$$*Leo** Mauris leo Phasellus vestibulum malesuada magna
		 quis tempor. auctor Ut pellentesque fermentum leo. Phasellus 
		 Morbi a mi et sem volutpat.`: {
			{`leo`: 6},
			{`phasellus`: 5},
			{`auctor`: 3},
			{`purus`: 3},
			{`blandit`: 2},
			{`morbi`: 2},
			{`nulla`: 2},
		},
	}, 7)
}

func runTestFrequencyAnalysis(t *testing.T, data map[string][]map[string]int, count int) {
	for text, expectedTopWords := range data {
		checkedListTopWords, _ := GetTopWords(text, count)
		if len(expectedTopWords) != len(checkedListTopWords) {
			t.Fatalf("expected number of top words does not match {expected_count: %d, actual_count: %d}",
				len(expectedTopWords), len(checkedListTopWords))
		}

		for index, checkedTopWords := range checkedListTopWords {
			for checkedTopWord, expectedRepeat := range checkedTopWords {
				checkedRepeat, ok := expectedTopWords[index][checkedTopWord]
				if !ok {
					t.Errorf("unexpected word as a result {unexpected_word: %s, actual_repeat: %d}",
						checkedTopWord, checkedListTopWords[index][checkedTopWord])
				}

				if checkedRepeat != expectedRepeat {
					t.Errorf("incorrect number of repetitions {word: %s, expected_repeat: %d, actual_repeat: %d}",
						checkedTopWord, expectedRepeat, checkedRepeat)
				}
			}
		}
	}
}
