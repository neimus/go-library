// Package textanalysis for text analysis
package textanalysis

import (
	"strings"

	"gitlab.com/neimus/go-library/text-analysis/pkg/service"
)

// GetTopWords returns the nth number of the most frequently occurring words,
// excluding word forms
func GetTopWords(text string, count int) ([]map[string]int, error) {

	analyzer := service.FrequencyAnalyzer{}
	words := strings.Fields(text)

	if err := analyzer.Init(len(words)); err != nil {
		return nil, err
	}

	for _, word := range words {
		analyzer.AddWord(word)
	}

	return analyzer.GetTopWords(count), nil
}
