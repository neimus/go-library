# Text-analysis
Библиотека для анализа текстов

### FrequencyAnalyzer
* `GetTopWords(count int) []map[string]int` - возвращает N-ое кол-во самых часто встречающихся слов без учета словоформ
> Есть поддержка Unicode
> 
> Учитываются только буквенные символы (без учета пунктуации, подчеркиваний и цифр)
>
> В случае одинакового кол-во повторений слов - они сортируются в алфавитном порядке
>
> НЕЯВНОЕ поведение: если в середине слова - есть буква или символ не разделенный пробелом - оно склеивается и считаеся за одно слово


## Установка
```bash
export GO111MODULE=on && go get -u gitlab.com/neimus/go-library/text-analysis
```

## Основные make-команды:
 * `make tools` - установка инструментов для разработки и не только (linters, easyjson ...)
 * `make test` - запуск тестов и проверка покрытия кода тестами
 * `make lint` - запуск проверки code style и ошибок в коде (перед запуском необходимо установить все инструменты `make tools`)
 * `make clean-all` - удаляет файлы созданные easyjson, coverage и очищает все зависимости `$GOPATH/pkg/mod`
 * `make tidy` - алиас для `go mod tidy`
 * `make` или `make help` - вызов справки по командам
