// Package service for text analysis
package service

import (
	"sort"
	"strings"
	"unicode"
)

// FrequencyAnalyzer analyzes frequently repeated words
type FrequencyAnalyzer struct {
	uniqWords []string
	buffer    map[string]int
}

// Len is the number of elements in the collection.
func (analyzer *FrequencyAnalyzer) Len() int {
	return len(analyzer.uniqWords)
}

// Less reports whether the element with
// index i should sort before the element with index j.
func (analyzer *FrequencyAnalyzer) Less(i, j int) bool {
	// Если они равны, то сортируем по алфавиту
	if analyzer.buffer[analyzer.uniqWords[i]] == analyzer.buffer[analyzer.uniqWords[j]] {
		words := []string{
			analyzer.uniqWords[i],
			analyzer.uniqWords[j],
		}
		sort.Strings(words)

		return words[0] == analyzer.uniqWords[i]
	}
	return analyzer.buffer[analyzer.uniqWords[i]] > analyzer.buffer[analyzer.uniqWords[j]]
}

// Swap swaps the elements with indexes i and j.
func (analyzer *FrequencyAnalyzer) Swap(i, j int) {
	analyzer.uniqWords[i], analyzer.uniqWords[j] = analyzer.uniqWords[j], analyzer.uniqWords[i]
}

// Init construct for FrequencyAnalyzer
func (analyzer *FrequencyAnalyzer) Init(countWords int) error {
	analyzer.uniqWords = make([]string, 0, countWords)
	analyzer.buffer = map[string]int{}

	return nil
}

// AddWord - adds a word to the frequency analyzer
func (analyzer *FrequencyAnalyzer) AddWord(word string) {
	word = analyzer.formatWord(word)
	if word == "" {
		return
	}
	word = strings.ToLower(word)

	if _, ok := analyzer.buffer[word]; !ok {
		analyzer.buffer[word] = 1
		analyzer.uniqWords = append(analyzer.uniqWords, word)
	} else {
		analyzer.buffer[word]++
	}
}

// GetTopWords returns count of frequently occurring words
func (analyzer *FrequencyAnalyzer) GetTopWords(count int) []map[string]int {
	sort.Sort(analyzer)
	if len(analyzer.uniqWords) > count {
		analyzer.uniqWords = analyzer.uniqWords[:count]
	}

	result := make([]map[string]int, 0, len(analyzer.uniqWords))
	for _, topWorld := range analyzer.uniqWords {
		result = append(result, map[string]int{topWorld: analyzer.buffer[topWorld]})
	}

	return result
}

func (analyzer *FrequencyAnalyzer) formatWord(word string) string {
	return strings.Join(
		strings.FieldsFunc(word, func(char rune) bool {
			return !unicode.IsLetter(char)
		}), "")
}
