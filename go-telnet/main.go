// Package main
package main

import (
	"context"
	"fmt"
	"os"
	"os/signal"
	"syscall"
	"time"

	"github.com/spf13/pflag"
	"gitlab.com/neimus/go-library/go-telnet/internal/logger"
	"gitlab.com/neimus/go-library/go-telnet/internal/telnet"
)

var timeout time.Duration
var host string
var port string

func init() {
	pflag.StringVar(&host, "host", "0.0.0.0", "host")
	pflag.StringVar(&port, "port", "8080", "port (0 - 65000)")
	pflag.DurationVar(&timeout, "timeout", 0, "timeout")
}

func main() {
	parseArgs()
	log := logger.NewConsoleLogger()

	interrupt := make(chan os.Signal, 1)
	signal.Notify(interrupt, os.Interrupt, syscall.SIGTERM)

	log.Debug(fmt.Sprintf("Timeout: %d | Host: %s | Port: %s\n", timeout, host, port))

	dialer := telnet.NewDialer(log, timeout)
	ctx, cancelFunc := context.WithCancel(context.Background())
	go dialer.Dial(ctx, host, port)

	select {
	case <-interrupt:
		cancelFunc()
		log.Error("program was interrupted by the user")
		os.Exit(1)
	case err := <-dialer.Error():
		cancelFunc()
		log.Error(err.Error())
		os.Exit(2)
	case <-dialer.Done():
	case <-ctx.Done():
	}
}

func parseArgs() {
	pflag.Parse()
	args := pflag.Args()
	nArgs := pflag.NArg()
	if nArgs == 1 {
		host = args[0]
	} else if nArgs >= 2 {
		host = args[0]
		port = args[1]
	}
}
