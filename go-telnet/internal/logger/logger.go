// Package logger - simple logger for outputting information to the console
package logger

import (
	"fmt"

	"github.com/logrusorgru/aurora"
)

//ILogger interface for simple logger for outputting information to the console
type ILogger interface {
	log(string)
	Error(message string)
	Warning(message string)
	Info(message string)
	Debug(message string)
}

// NewConsoleLogger construct for ILogger
func NewConsoleLogger() ILogger {
	return &consoleLogger{}
}

type consoleLogger struct{}

func (l *consoleLogger) log(message string) {
	fmt.Println(message)
}

func (l *consoleLogger) Error(message string) {
	l.log(aurora.Bold(aurora.Red(message)).String())
}

func (l *consoleLogger) Warning(message string) {
	l.log(aurora.Bold(aurora.Yellow(message)).String())
}

func (l *consoleLogger) Info(message string) {
	l.log(aurora.Blue(message).String())
}

func (l *consoleLogger) Debug(message string) {
	l.log(aurora.Gray(15, message).String())
}
