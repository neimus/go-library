// Package telnet client (handles input/output)
package telnet

import (
	"bufio"
	"context"
	"errors"
	"fmt"
	"io"
	"net"
	"os"
)

const delimiter = '\n'

// IClient interface for telnet client (handles input/output)
type IClient interface {
	AcceptRemoteData(ctx context.Context)
	AcceptLocalData(ctx context.Context)
	Done() <-chan struct{}
	Error() <-chan error
}

type client struct {
	chDone  chan struct{}
	chError chan error

	stdRemote  *bufio.ReadWriter
	stdLocal   *bufio.ReadWriter
	connection net.Conn
}

// NewClient - constructor for IClient
func NewClient(connection net.Conn) IClient {
	client := client{
		connection: connection,
		stdRemote:  bufio.NewReadWriter(bufio.NewReader(connection), bufio.NewWriter(connection)),
		stdLocal:   bufio.NewReadWriter(bufio.NewReader(os.Stdin), bufio.NewWriter(os.Stdout)),
	}
	client.init()

	return &client
}

func (c *client) init() {
	c.chDone = make(chan struct{}, 2)
	c.chError = make(chan error, 1)
}

// AcceptRemoteData - starts receiving from a remote client
// displays messages in stdout
func (c *client) AcceptRemoteData(ctx context.Context) {
	if c.stdRemote == nil {
		c.chError <- errors.New("remote input/output not set")
		return
	}

	go c.acceptData(c.stdRemote, c.stdLocal)
	c.wait(ctx)
}

// AcceptLocalData - starts receiving from a local client (stdout),
// output is to a remote client
func (c *client) AcceptLocalData(ctx context.Context) {
	if c.stdLocal == nil {
		c.chError <- errors.New("local input/output not set")
		return
	}

	go c.acceptData(c.stdLocal, c.stdRemote)
	c.wait(ctx)
}

// Done - return Done channel
func (c *client) Done() <-chan struct{} {
	return c.chDone
}

// Done - return Error channel
func (c *client) Error() <-chan error {
	return c.chError
}

func (c *client) getQuitCommand() map[string]bool {
	return map[string]bool{
		fmt.Sprintf("quit%c", delimiter): true,
		fmt.Sprintf("exit%c", delimiter): true,
	}
}

func (c *client) acceptData(in *bufio.ReadWriter, out *bufio.ReadWriter) {
	defer c.acceptanceCompleted()
	quitCommand := c.getQuitCommand()
	for {
		message, err := in.ReadString(delimiter)
		exit, ok := quitCommand[message]
		if (ok && exit) || err == io.EOF {
			_ = c.write(out, "Connection is closed by the remote side\n")
			return
		}

		if err != nil {
			c.chError <- fmt.Errorf("setError reading data from stdin (%w)", err)
			return
		}

		if err = c.write(out, message); err != nil {
			c.chError <- err
			return
		}
	}
}

func (c *client) acceptanceCompleted() {
	c.chDone <- struct{}{}
}

func (c *client) write(std *bufio.ReadWriter, message string) error {
	_, err := std.WriteString(message)
	if err != nil {
		return fmt.Errorf("setError while displaying a message (%w)", err)
	}

	if err := std.Flush(); err != nil {
		return fmt.Errorf("setError while displaying a message (%w)", err)
	}

	return nil
}

func (c *client) wait(ctx context.Context) {
	select {
	case <-c.chDone:
	case <-c.chError:
	case <-ctx.Done():
	}
}
