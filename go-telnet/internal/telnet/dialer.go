// Package telnet dialer
package telnet

import (
	"context"
	"errors"
	"fmt"
	"net"
	"time"

	"gitlab.com/neimus/go-library/go-telnet/internal/logger"
)

const defaultTimeout = 30 * time.Second
const network = "tcp"

//IDialer interface
type IDialer interface {
	Dial(ctx context.Context, host string, port string)
	Done() <-chan struct{}
	Error() <-chan error
}

type dialer struct {
	chError chan error
	chDone  chan struct{}

	netDialer net.Dialer
	timeout   time.Duration
	log       logger.ILogger
}

// NewDialer - constructor for IDialer
func NewDialer(logger logger.ILogger, timeout time.Duration) IDialer {
	t := dialer{log: logger, timeout: timeout}
	t.init()
	return &t
}

func (d *dialer) init() {
	d.chError = make(chan error)
	d.chDone = make(chan struct{})
}

// Dial connects to a remote server
func (d *dialer) Dial(ctx context.Context, host string, port string) {
	defer close(d.chDone)
	if d.log == nil {
		d.chError <- errors.New("log not set")
		return
	}
	if err := checkPort(port); err != nil {
		d.chError <- err
		return
	}

	var dialCtx context.Context
	var dialCancel context.CancelFunc
	if d.timeout == 0 {
		dialCtx, dialCancel = context.WithTimeout(ctx, defaultTimeout)
	} else {
		dialCtx, dialCancel = context.WithTimeout(ctx, d.timeout)
	}
	defer dialCancel()

	conn, err := d.netDialer.DialContext(dialCtx, network, net.JoinHostPort(host, port))
	if err != nil {
		d.chError <- err
		return
	}

	d.handleConnection(ctx, conn)
}

// Done - return Done channel
func (d *dialer) Done() <-chan struct{} {
	return d.chDone
}

// Done - return Error channel
func (d *dialer) Error() <-chan error {
	return d.chError
}

func (d *dialer) handleConnection(ctx context.Context, conn net.Conn) {
	defer d.connectionClose(conn)
	d.log.Info(fmt.Sprintf("Connected %s", conn.RemoteAddr()))

	clientConn := NewClient(conn)

	go clientConn.AcceptRemoteData(ctx)
	go clientConn.AcceptLocalData(ctx)

	select {
	case err := <-clientConn.Error():
		d.chError <- err
	case <-clientConn.Done():
	case <-ctx.Done():
	}
}

func (d *dialer) connectionClose(conn net.Conn) {
	if conn == nil {
		return
	}

	if err := conn.Close(); err != nil {
		d.chError <- err
	}
	d.log.Info(fmt.Sprintf("Closing dialer with %s", conn.RemoteAddr()))
}

func checkPort(port string) error {
	_, err := net.LookupPort(network, port)
	if err != nil {
		return fmt.Errorf("wrong format for port: %s (%w)", port, err)
	}
	return nil
}
