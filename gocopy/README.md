## Утилита копирования файлов (+ возможность докопировать файл) с выводом в консоль прогресса копирования. 
> корректная обработка ситуацию, когда offset или offset+limit за пределами source файла.

Пример использования:
# копирует 2К из source в dest, пропуская 1K данных
```bash
gocopy --­from /path/to/source --­to /path/to/dest --­offset 1024 --­limit 2048
```

### Установка
```bash
export GO111MODULE=on && go get -u gitlab.com/neimus/go-library/gocopy
```

### Основные make-команды:
 * `make install` - установка всех зависимостей, создания бинарников
 * `make dependency` - устанавливает все зависимости в `$GOPATH/pkg/mod`
 * `make compress` - компрессия бинарников (требуется `upx`)
 * `make tools` - установка инструментов для разработки и не только (linters, easyjson ...)
 * `make test` - запуск тестов и проверка покрытия кода тестами
 * `make lint` - запуск проверки code style и ошибок в коде (перед запуском необходимо установить все инструменты `make tools`)
 * `make clean-all` - удаляет файлы созданные easyjson, coverage и очищает все зависимости `$GOPATH/pkg/mod`
 * `make tidy` - алиас для `go mod tidy`
 * `make` или `make help` - вызов справки по командам

