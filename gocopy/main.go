// Package main
package main

import (
	"flag"
	"fmt"
	"os"
	"syscall"

	"github.com/logrusorgru/aurora"
	"github.com/schollz/progressbar/v2"
	pcopy "gitlab.com/neimus/go-library/gocopy/pkg/service"
)

var offset uint64
var limit uint64
var pathFrom string
var pathTo string

var args map[string]string

func init() {
	args = map[string]string{
		"offset": "offset in input file",
		"limit":  "limit in input file",
		"from":   "source file path",
		"to":     "path to copy the file",
	}

	flag.Uint64Var(&offset, "offset", 0, args["offset"])
	flag.Uint64Var(&limit, "limit", 0, args["limit"])
	flag.StringVar(&pathFrom, "from", "", args["from"])
	flag.StringVar(&pathTo, "to", "", args["to"])
}

func main() {
	flag.Parse()
	pc := pcopy.Pcopy{
		Offset:   int64(offset),
		Limit:    int64(limit),
		PathFrom: pathFrom,
		PathTo:   pathTo,
	}
	pc.TerminateOnSysCall(syscall.SIGTERM)

	sizeWrite := pc.CopyFiles()
	if sizeWrite > 0 {
		bar := progressbar.NewOptions64(sizeWrite, progressbar.OptionSetBytes64(sizeWrite))
		_ = bar.Add(1)

		var write int
		for {
			select {
			case <-pc.Done:
				_ = bar.Finish()
				fmt.Printf("\n%s %s -> %s \n", aurora.Green("successfully copied"),
					aurora.Bold(pathFrom), aurora.Bold(pathTo))
				return
			case err := <-pc.Error:
				exitWithError(err, 1)
			case write = <-pc.SizeWriteByte:
				_ = bar.Add(write)
			}
		}
	} else {
		fmt.Println(aurora.Yellow("nothing to copy..."))
	}
}

func exitWithError(err error, code int) {
	fmt.Println()
	fmt.Println(aurora.Bold(aurora.Red(err)))
	printHelp()
	os.Exit(code)
}

func printHelp() {
	fmt.Println("Arguments:")
	for arg, description := range args {
		fmt.Printf("%s		- %s\n", aurora.Green(arg), aurora.Gray(20, description))
	}
}
