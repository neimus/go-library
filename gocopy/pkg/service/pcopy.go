// Package pcopy - partial copy
package pcopy

import (
	"errors"
	"io"
	"os"
	"os/signal"
)

const maxBuffer = 1024

// Pcopy struct
type Pcopy struct {
	Offset   int64
	Limit    int64
	PathFrom string
	PathTo   string

	Done          chan struct{}
	SizeWriteByte chan int
	Error         chan error
	interrupt     chan os.Signal

	srcFileSize int64
	srcPerm     os.FileMode

	fileSrc *os.File
	fileDst *os.File
}

// CopyFiles - start copying files
func (pc *Pcopy) CopyFiles() int64 {
	pc.Error = make(chan error, 1)
	pc.SizeWriteByte = make(chan int, 1)
	pc.Done = make(chan struct{})

	sizeWrite, err := pc.parseFileInfo()
	if err != nil {
		pc.Error <- err
		return 0
	}

	if sizeWrite <= 0 {
		close(pc.Done)
		return 0
	}

	go func(fc *Pcopy, sizeWrite int64) {
		if err := fc.openFileSrc(); err != nil {
			fc.Error <- err
			return
		}
		defer fc.closeFile(fc.fileSrc)

		if err := fc.openFileDst(); err != nil {
			fc.Error <- err
			return
		}
		defer fc.closeFile(fc.fileDst)

		if err := fc.copy(sizeWrite); err != nil {
			fc.Error <- err
			return
		}

		close(fc.Done)
	}(pc, sizeWrite)

	return sizeWrite
}

func (pc *Pcopy) parseFileInfo() (int64, error) {
	var err error
	fileInfo, err := os.Stat(pc.PathFrom)
	if err != nil {
		return 0, err
	}

	pc.srcPerm = fileInfo.Mode().Perm()
	pc.srcFileSize = fileInfo.Size()

	if pc.Offset >= pc.srcFileSize {
		return 0, nil
	}

	sizeWrite := pc.srcFileSize - pc.Offset
	if pc.Limit > 0 && pc.Limit < sizeWrite {
		sizeWrite = pc.Limit
	}

	return sizeWrite, nil
}

func (pc *Pcopy) copy(sizeRead int64) error {
	var buf []byte
	if sizeRead <= maxBuffer {
		buf = make([]byte, sizeRead)
	} else {
		buf = make([]byte, maxBuffer)
	}

	totalRead := int64(0)

	for totalRead < sizeRead {
		select {
		case <-pc.interrupt:
			return errors.New("program was interrupted by the user")
		default:
			read, err := pc.fileSrc.Read(buf)
			totalRead += int64(read)

			if err == io.EOF {
				break
			} else if err != nil {
				return err
			}

			write, err := pc.fileDst.Write(buf[:read])
			if err != nil {
				return err
			}
			pc.SizeWriteByte <- write
		}
	}

	return nil
}

// TerminateOnSysCall sets the OS signal to stop all tasks
func (pc *Pcopy) TerminateOnSysCall(sig os.Signal) {
	pc.interrupt = make(chan os.Signal, 1)
	signal.Notify(pc.interrupt, os.Interrupt, sig)
}

func (pc *Pcopy) openFileSrc() error {
	var err error

	pc.fileSrc, err = os.Open(pc.PathFrom)
	if err != nil {
		return err
	}
	_, err = pc.fileSrc.Seek(pc.Offset, io.SeekStart)
	if err != nil {
		return err
	}
	if pc.Limit > 0 {
		io.LimitReader(pc.fileSrc, pc.Limit)
	}

	return nil
}

func (pc *Pcopy) openFileDst() error {
	var err error

	pc.fileDst, err = os.OpenFile(pc.PathTo, os.O_RDWR|os.O_CREATE, pc.srcPerm)
	if err != nil && !os.IsNotExist(err) {
		return err
	}

	_, err = pc.fileDst.Seek(pc.Offset, io.SeekStart)
	if err != nil {
		return err
	}

	return nil
}

func (pc *Pcopy) closeFile(file *os.File) {
	_ = file.Close()
}
