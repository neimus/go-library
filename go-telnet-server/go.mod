module gitlab.com/neimus/go-library/go-telnet

go 1.13

require (
	github.com/logrusorgru/aurora v0.0.0-20191116043053-66b7ad493a23
	github.com/spf13/pflag v1.0.5
)
