// Package telnet listener receiving messages on a host with a specific port
package telnet

import (
	"context"
	"errors"
	"fmt"
	"net"
	"time"

	"gitlab.com/neimus/go-library/go-telnet/internal/service/logger"
)

const welcomeMessageTemplate string = "Welcome to telnet! \n Local address: %s \n Remote address: %s \n"
const defaultTimeout = 2 * time.Minute

//IListener interface for receiving messages on a host with a specific port
type IListener interface {
	Listen(ctx context.Context, host string, port string)
	Done() <-chan struct{}
	Error() <-chan error
}

type listener struct {
	chError chan error
	chDone  chan struct{}

	listener     net.Listener
	log          logger.ILogger
	timeout      time.Duration
	multiConnect bool
}

// NewListener - constructor for IListener
func NewListener(logger logger.ILogger, timeout time.Duration, multiConnect bool) IListener {
	t := listener{log: logger, timeout: timeout, multiConnect: multiConnect}
	t.init()
	return &t
}

func (l *listener) init() {
	l.chError = make(chan error)
	l.chDone = make(chan struct{})
}

// Listen - starts receiving messages on a host with a specific port
func (l *listener) Listen(ctx context.Context, host string, port string) {
	if l.log == nil {
		l.chError <- errors.New("log not set")
		return
	}
	if err := l.checkPort(port); err != nil {
		l.chError <- err
		return
	}
	if l.timeout == 0 {
		l.timeout = defaultTimeout
	}

	var err error
	l.listener, err = net.Listen("tcp", net.JoinHostPort(host, port))
	defer l.listenerClose()
	if err != nil {
		l.chError <- err
		return
	}

	if l.multiConnect {
		for {
			l.listenAccept(ctx)
		}
	} else {
		l.listenAccept(ctx)
	}
}

// Done - return Done channel
func (l *listener) Done() <-chan struct{} {
	return l.chDone
}

// Done - return Error channel
func (l *listener) Error() <-chan error {
	return l.chError
}

func (l *listener) listenAccept(ctx context.Context) {
	conn, err := l.listener.Accept()
	if err != nil {
		l.chError <- err
	}
	go l.handleConnection(ctx, conn)
}

func (l *listener) checkPort(port string) error {
	_, err := net.LookupPort("tcp", port)
	if err != nil {
		return fmt.Errorf("wrong format for port: %s (%w)", port, err)
	}
	return nil
}

func (l *listener) handleConnection(ctx context.Context, conn net.Conn) {
	defer l.connectionClose(conn)
	l.log.Info(fmt.Sprintf("Connected %s", conn.RemoteAddr()))

	_, err := conn.Write([]byte(fmt.Sprintf(welcomeMessageTemplate, conn.LocalAddr(), conn.RemoteAddr())))
	if err != nil {
		l.setError(err)
		return
	}

	clientConn := NewClient(conn, l.timeout)

	go clientConn.AcceptRemoteData(ctx)
	go clientConn.AcceptLocalData(ctx)

	select {
	case err := <-clientConn.Error():
		l.setError(err)
	case <-clientConn.Done():
	case <-ctx.Done():
	}
}

func (l *listener) setError(err error) {
	if l.multiConnect {
		l.log.Error(err.Error())
	} else {
		l.chError <- err
	}
}

func (l *listener) listenerClose() {
	if l.listener == nil {
		return
	}
	if err := l.listener.Close(); err != nil {
		l.chError <- err
	}
}

func (l *listener) connectionClose(conn net.Conn) {
	if conn == nil {
		return
	}

	if err := conn.Close(); err != nil {
		l.chError <- err
	}
	l.log.Info(fmt.Sprintf("Closing listener with %s", conn.RemoteAddr()))

	if !l.multiConnect {
		close(l.chDone)
	}
}
